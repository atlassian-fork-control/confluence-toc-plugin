/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.toc;

import java.io.IOException;

import com.atlassian.confluence.macro.MacroExecutionException;

/**
 *
 * @author David Peterson
 */
public class FlatHandler implements OutputHandler {
	
    private boolean firstItemWritten = false;
    private String pre;
    private String mid;
    private String post;
    
    public FlatHandler(String separatorName) throws MacroExecutionException
    {
    	try
    	{
    		SeparatorType separatorType = SeparatorType.valueOfSeparator(separatorName);
    		pre = separatorType.getPre();
    		mid = separatorType.getMid();
    		post = separatorType.getPost();
    	}
    	catch (Exception e)
    	{
    		pre = "";
    		mid = separatorName;
    		post = "";
    	}
    }

    @Override
    public String appendStyle( Appendable out )
    {
        // No specific default styles for flat layout...
        return null;
    }

    @Override
    public void appendIncLevel( Appendable out )
    {
        // Do nothing...
    }

    @Override
    public void appendDecLevel( Appendable out )
    {
        // Do nothing...
    }

    @Override
    public void appendPrefix( Appendable out ) throws IOException
    {
        out.append( pre );
    }

    @Override
    public void appendPostfix( Appendable out ) throws IOException
    {
        if (!firstItemWritten)
            return;

        out.append( post );
    }

    @Override
    public void appendSeparator( Appendable out ) throws IOException
    {
        // ignore if no headings have been written yet.
        if (!firstItemWritten)
            return;

        out.append( mid );
    }

    /*
     * (non-Javadoc)
     *
     * @see org.randombits.confluence.toc.OutputHandler#appendHeading(java.lang.StringBuffer,
     *      java.lang.String)
     */
    @Override
    public void appendHeading( Appendable out, String string ) throws IOException
    {
        out.append( string );
        firstItemWritten = true;
    }
}
