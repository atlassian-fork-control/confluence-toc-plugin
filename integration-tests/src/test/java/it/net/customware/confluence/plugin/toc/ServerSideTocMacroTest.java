package it.net.customware.confluence.plugin.toc;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import static java.util.Arrays.asList;

/**
 * Tests for conditions where the TOC macro is rendered using the server-side implementation
 */
public class ServerSideTocMacroTest extends AbstractConfluencePluginWebTestCase
{
    private TocTester tocTester;
    private String testSpaceKey;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        tocTester = new TocTester(tester);
        testSpaceKey = tocTester.createTestSpace();
    }

    public void testThatTocMacroInIncludedPageOnlyRendersHeadersFromTheIncludedPage()
    {
        final PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Page");
        pageHelper.setContent(
                "{toc}\n" +
                        "h1. Heading 1\n" +
                        "h2. Heading 1.1\n" +
                        "h3. Heading 1.1.1");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        final PageHelper includePageHelper = getPageHelper();
        includePageHelper.setSpaceKey(testSpaceKey);
        includePageHelper.setTitle("Include Page for " + pageHelper.getTitle());
        includePageHelper.setContent(
                "{include:" + pageHelper.getTitle() + "}\n" +
                "h1. Heading 2"
        );

        assertTrue(includePageHelper.create());

        /* Check if page is included properly */
        gotoPage("/pages/viewpage.action?pageId=" + includePageHelper.getId());

        // Links should have containing page's anchor
        tocTester.verifyHeadings(
                "//div[contains(@class,'toc-macro')]/ul",
                asList(
                        new Heading("Heading 1", "#IncludePageforTOCPage-Heading1",
                                asList(
                                        new Heading("Heading 1.1", "#IncludePageforTOCPage-Heading1.1",
                                                asList(
                                                        new Heading("Heading 1.1.1", "#IncludePageforTOCPage-Heading1.1.1")
                                                )
                                        )
                                )
                        )
                )
        );
    }


}
