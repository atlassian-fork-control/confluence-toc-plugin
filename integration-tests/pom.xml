<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.atlassian.confluence.plugins.toc</groupId>
        <artifactId>toc-plugin-project</artifactId>
        <version>5.0.4-SNAPSHOT</version>
    </parent>

    <artifactId>toc-plugin-integration-tests</artifactId>
    <version>5.0.4-SNAPSHOT</version>
    <packaging>jar</packaging>

    <name>Table of Contents Plugin Integration Tests</name>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.confluence</groupId>
                <artifactId>confluence-plugins-platform-test-pom</artifactId>
                <version>${confluence.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    <dependencies>
        <dependency>
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-utils</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-stateless-test-runner</artifactId>
            <scope>test</scope>
            <exclusions>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>log4j-over-slf4j</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.slf4j</groupId>
                    <artifactId>slf4j-simple</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-webdriver-pageobjects</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-test-utils</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.selenium</groupId>
            <artifactId>atlassian-webdriver-core</artifactId>
            <scope>test</scope>
        </dependency>
        <!-- For Legacy tests only -->
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-test-support</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence-java-api</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-library</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence.plugin</groupId>
            <artifactId>func-test-package</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.util.concurrent</groupId>
            <artifactId>atlassian-util-concurrent</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>confluence-maven-plugin</artifactId>
                <extensions>true</extensions>
                <executions>
                    <execution>
                        <id>webdriver-tests</id>
                        <phase>integration-test</phase>
                        <goals>
                            <goal>integration-test</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <containerId>${container}</containerId>
                    <jvmArgs>${jvm.args}</jvmArgs>
                    <output>${project.build.directory}/output.log</output>
                    <productVersion>${confluence.version}</productVersion>
                    <productDataVersion>${confluence.data.version}</productDataVersion>
                    <pluginArtifacts>
                        <pluginArtifact>
                            <groupId>com.atlassian.confluence.plugins.toc</groupId>
                            <artifactId>toc-plugin</artifactId>
                            <version>${project.version}</version>
                        </pluginArtifact>
                    </pluginArtifacts>
                    <systemPropertyVariables>
                        <atlassian.dev.mode>${atlassian.dev.mode}</atlassian.dev.mode>
                        <webdriver.browser>${webdriver.browser}</webdriver.browser>
                    </systemPropertyVariables>
                </configuration>
            </plugin>
            <plugin><!-- Export webdriver tests - see https://pug.jira.com/wiki/display/CONFDEV/2013/05/06/Exporting+sanity+tests+from+a+plugin+to+run+in+core -->
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>2.4</version>
                <executions>
                    <execution>
                        <id>test-jar</id>
                        <goals>
                            <goal>test-jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>flaky</id>
            <properties>
                <ci.tests.skipAfterFailureCount>10</ci.tests.skipAfterFailureCount>
                <ci.tests.rerunFailingTestsCount>2</ci.tests.rerunFailingTestsCount>
            </properties>
            <build>
                <plugins>
                    <plugin>
                        <groupId>com.googlecode.maven-download-plugin</groupId>
                        <artifactId>download-maven-plugin</artifactId>
                        <version>${download.maven.plugin.version}</version>
                        <executions>
                            <execution>
                                <id>download-flaky-configuration</id>
                                <phase>process-test-resources</phase>
                                <goals>
                                    <goal>wget</goal>
                                </goals>
                                <configuration>
                                    <url>https://s3-ap-southeast-2.amazonaws.com/confluence-server/bamboo/issuecreator_plugins_config.json</url>
                                    <outputDirectory>${project.build.testOutputDirectory}</outputDirectory>
                                    <overwrite>true</overwrite>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <configuration>
                            <skipAfterFailureCount>${ci.tests.skipAfterFailureCount}</skipAfterFailureCount>
                            <rerunFailingTestsCount>${ci.tests.rerunFailingTestsCount}</rerunFailingTestsCount>
                            <properties>
                                <property>
                                    <name>listener</name>
                                    <value>com.atlassian.test.reporting.JUnitFlakyTestListener</value>
                                </property>
                            </properties>
                            <systemPropertyVariables>
                                <junitflakylistener.runnerId>${env.bamboo_buildResultKey}</junitflakylistener.runnerId>
                                <junitflakylistener.branchName>${env.bamboo_planRepository_1_branchName}</junitflakylistener.branchName>
                                <!-- issue creation kill switch; by default, issues will be created on branch builds as well -->
                                <junitflakylistener.trackFlakyTest>true</junitflakylistener.trackFlakyTest>
                                <junitflakylistener.flakyTestFilePath>target/failsafe-reports/flakey-tests.txt</junitflakylistener.flakyTestFilePath>
                                <junitflakylistener.jiraProjectConfigPath>issuecreator_plugins_config.json</junitflakylistener.jiraProjectConfigPath>
                                <junitflakylistener.jiraUserName>${env.bamboo_issuecreator_jiraUserName}</junitflakylistener.jiraUserName>
                                <junitflakylistener.jiraPassword>${env.bamboo_issuecreator_jiraPassword}</junitflakylistener.jiraPassword>
                            </systemPropertyVariables>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
            <dependencies>
                <dependency>
                    <groupId>com.atlassian</groupId>
                    <artifactId>flaky-test-reporter</artifactId>
                    <version>${flaky.test.reporter.version}</version>
                    <scope>test</scope>
                </dependency>
                <dependency>
                    <groupId>com.googlecode.maven-download-plugin</groupId>
                    <artifactId>download-maven-plugin</artifactId>
                    <version>${download.maven.plugin.version}</version>
                    <scope>test</scope>
                </dependency>
            </dependencies>
        </profile>
    </profiles>
</project>
